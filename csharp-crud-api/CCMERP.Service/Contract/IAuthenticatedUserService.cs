﻿
namespace CCMERP.Service.Contract
{
    public interface IAuthenticatedUserService
    {
        string UserId { get; }
    }
}
