﻿using CCMERP.Domain.Common;
using CCMERP.Domain.DefaultValues;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
namespace CCMERP.Service.Features.DefaultValueService.Queries
{
    public class GetAllDefaultValuesQuery : IRequest<Response<GetAllDefaultValueResponse>> 
    {
        
        public GetAllDefaultValuesQuery()
        {

        }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public int DefaultYear { get; private set; }

        public class GetAllDefaultValuesQueryHandler : IRequestHandler<GetAllDefaultValuesQuery, Response<GetAllDefaultValueResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllDefaultValuesQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllDefaultValueResponse>> Handle(GetAllDefaultValuesQuery request, CancellationToken cancellationToken)
            {

                GetAllDefaultValueResponse getAllDefaultValuesResponse = new GetAllDefaultValueResponse();
                try
                {
                    var DefaultList = await
                        (
                       from d in _context.defaultvalues
                           //join cm in _context.country_master on p.CountryID equals cm.CountryID
                       select new DefaultValue()
                       {
                           DefaultValueID = d.DefaultValueID,
                           DefaultName = d.DefaultName,
                           DefaultValue1 = d.DefaultValue1,
                           DefaultValue2 = d.DefaultValue2,
                           DefaultValue3 = d.DefaultValue3,
                           DefaultValue4 = d.DefaultValue4,
                           DefaultDate = d.DefaultDate,
                           DefaultCity = d.DefaultCity,
                           DefaultTime = DateTime.Now.ToString("hh:mm:yy"),
                           DefaultPhone = d.DefaultPhone,
                           DefaultID = d.DefaultID,
                           DefaultValue5 = d.DefaultValue5,
                           DefaultValue6 = d.DefaultValue6,
                           DefaultYear = DateTime.Now.ToString("yyyy"),
                           DefaultText = d.DefaultText,
                           DefaultText2 = d.DefaultText2,
                           DefaultText3 = d.DefaultText3,

                           //PrivilegePrice = p.PrivilegePrice,
                           //PrivilegeDate = DateTime.Now,
                           //CountryID = p.CountryID,
                           // CountryName = cm.CountryName

                       }).Distinct().ToListAsync();
                    if (DefaultList.Count == 0)
                    {

                        return new Response<GetAllDefaultValueResponse>(getAllDefaultValuesResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllDefaultValuesResponse.TotalItems = DefaultList.Count;
                        getAllDefaultValuesResponse.TotalPages = (int)Math.Ceiling(getAllDefaultValuesResponse.TotalItems / (double)request.PageSize);
                        getAllDefaultValuesResponse.DefaultValues = DefaultList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllDefaultValueResponse>(getAllDefaultValuesResponse, "Success", true);
                    }
                }
                catch (Exception ex)
                {
                    var error = ex;

                    return new Response<GetAllDefaultValueResponse>(getAllDefaultValuesResponse, "Exception", false);
                }


            }
        }



    }
}
    

