﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValueService.Queries
{
    public class GetDefaultValuesByIdQuery : IRequest<Response<DefaultValue>>
    {
        public int DefaultValueID { get; set; }
        public class GetDefaultValuesByIdQueryHandler : IRequestHandler<GetDefaultValuesByIdQuery, Response<DefaultValue>>

        {
            private readonly IdentityContext _context;

            public GetDefaultValuesByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<DefaultValue>> Handle(GetDefaultValuesByIdQuery request, CancellationToken cancellationToken)
            {

                DefaultValue defaultvalue = new();
                try
                {

                    defaultvalue = (from t1 in _context.defaultvalues

                                    where t1.DefaultValueID == request.DefaultValueID
                                    select new DefaultValue
                                    {
                                        DefaultValueID = t1.DefaultValueID,
                                        DefaultName = t1.DefaultName,
                                        DefaultValue1 = t1.DefaultValue1,
                                        DefaultValue2 = t1.DefaultValue2,
                                        DefaultValue3 = t1.DefaultValue3,
                                        DefaultDate = t1.DefaultDate,
                                        DefaultValue4 = t1.DefaultValue4,
                                        DefaultTime = DateTime.Now.ToString("hh:mm:yy"),
                                        DefaultCity = t1.DefaultCity,
                                        DefaultPhone = t1.DefaultPhone,
                                        DefaultID = t1.DefaultID,
                                        DefaultValue5 = t1.DefaultValue5,
                                        DefaultValue6 = t1.DefaultValue6,
                                        DefaultYear = DateTime.Now.ToString("yyyy"),
                                        DefaultText = t1.DefaultText,
                                        DefaultText2 = t1.DefaultText2,
                                        DefaultText3 = t1.DefaultText3,


                                    }).FirstOrDefault();

                    if (defaultvalue != null)
                    {
                        return await Task.FromResult(new Response<DefaultValue>(defaultvalue, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<DefaultValue>(defaultvalue, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<DefaultValue>(defaultvalue, "Exception", false));
                }

            }
        }



    }
}


