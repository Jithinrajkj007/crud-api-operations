﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValueService.Commands
{
    public class CreateDefaultValuesCommand : IRequest<Response<int>>
    {
        [Required]
        public int DefaultValueID { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string DefaultName { get; set; }
        public decimal DefaultValue1 { get; set; }
        public float DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public DateTime DefaultDate { get; set; }
        public string DefaultValue4 { get; set; }
        public DateTime DefaultTime { get; set; }
        public string DefaultCity { get; set; }
        public long DefaultPhone { get; set; }
        public byte DefaultID { get; set; }
        public int DefaultValue5 { get; set; }
        public int DefaultValue6 { get; set; }
        public DateTime DefaultYear { get; set; }
        public string DefaultText { get; set; }
        public string DefaultText2 { get; set; }
        public string DefaultText3 { get; set; }


        public class CreateDefaultValuesCommandHandler : IRequestHandler<CreateDefaultValuesCommand, Response<int>>
        {
            private readonly IdentityContext _context;

            //private readonly ITransactionDbContext _tcontext;
            public CreateDefaultValuesCommandHandler(IdentityContext context)//, ITransactionDbContext tcontext)
            {
                _context = context;
                //_tcontext = tcontext;
            }
            public async Task<Response<int>> Handle(CreateDefaultValuesCommand request, CancellationToken cancellationToken)
            {

                try
                {


                    var pre = _context.defaultvalues.Where(a => a.DefaultName.ToLower() == request.DefaultName.ToLower()).ToList();
                    if (pre.Count <= 0)
                    {

                        DefaultValue defaultvalue = new()
                        {

                            DefaultValueID = request.DefaultValueID,
                            DefaultName = request.DefaultName,
                            DefaultValue1 = request.DefaultValue1,
                            DefaultValue2 = request.DefaultValue2,
                            DefaultValue3 = request.DefaultValue3,
                            DefaultDate = request.DefaultDate,
                            DefaultValue4 = request.DefaultValue4,
                            DefaultTime = DateTime.Now.ToString("hh:mm:ss"),
                            DefaultCity = request.DefaultCity,
                            DefaultPhone = request.DefaultPhone,
                            DefaultID = request.DefaultID,
                            DefaultValue5 = request.DefaultValue5,
                            DefaultValue6 = request.DefaultValue6,
                            DefaultYear = DateTime.Now.ToString("yyyy"),
                            DefaultText = request.DefaultText,
                            DefaultText2 = request.DefaultText2,
                            DefaultText3 = request.DefaultText3,

                        };

                        _context.defaultvalues.Add(defaultvalue);
                        await _context.SaveChangesAsync();

                        return new Response<int>(defaultvalue.DefaultValueID, "Success", true);
                    }
                    else
                    {
                        return new Response<int>(0, "A defaultvalue with the same name already exists", false);
                    }
                }
                catch (Exception ex)
                {

                    var currentExpection = ex;
                    return new Response<int>(0, "Exception", false);
                }

            }
        }



    }
}