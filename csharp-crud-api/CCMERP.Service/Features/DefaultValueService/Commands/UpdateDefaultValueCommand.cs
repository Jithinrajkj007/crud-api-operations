﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValueService.Commands
{
    public class UpdateDefaultValueCommand : IRequest<Response<int>>
    {
        [Required]
        public int DefaultValueID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string DefaultName { get; set; }
        
        public decimal DefaultValue1 { get; set; }
        public float DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public DateTime DefaultDate { get; set; }
        public string DefaultValue4 { get; set; }
        public DateTime DefaultTime { get; set; }
        public string DefaultCity { get; set; }
        public long DefaultPhone { get; set; }
        public byte DefaultID { get; set; }
        public int DefaultValue5 { get; set; }
        public int DefaultValue6 { get; set; }
        public DateTime DefaultYear { get; set; }
        public string DefaultText { get; set; }
        public string DefaultText2 { get; set; }
        public string DefaultText3 { get; set; }
        public class UpdateDefaultValueCommandHandler : IRequestHandler<UpdateDefaultValueCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdateDefaultValueCommandHandler(IdentityContext context)
            {
                _context = context;
            }

            //public DateTime DefaultTime { get; private set; }

            public async Task<Response<int>> Handle(UpdateDefaultValueCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var pre = _context.defaultvalues.Where(a => a.DefaultValueID == request.DefaultValueID).FirstOrDefault();

                    if (pre == null)
                    {
                        return new Response<int>(0, "No default value found", true);
                    }
                    else
                    {

                        pre.DefaultValueID = request.DefaultValueID;
                        pre.DefaultName = request.DefaultName;
                        pre.DefaultValue1 = request.DefaultValue1;
                        pre.DefaultValue2 = request.DefaultValue2;
                        pre.DefaultValue3 = request.DefaultValue3;
                        pre.DefaultDate = request.DefaultDate;
                        pre.DefaultValue4 = request.DefaultValue4;
                        pre.DefaultTime = DateTime.Now.ToString("hh:mm:ss");
                        pre.DefaultCity = request.DefaultCity;
                        pre.DefaultPhone = request.DefaultPhone;
                        pre.DefaultID = request.DefaultID;
                        pre.DefaultValue5 = request.DefaultValue5;
                        pre.DefaultValue6 = request.DefaultValue6;
                        pre.DefaultYear = DateTime.Now.ToString("yyyy");
                        pre.DefaultText = request.DefaultText;
                        pre.DefaultText2 = request.DefaultText2;
                        pre.DefaultText3 = request.DefaultText3;

                        _context.defaultvalues.Update(pre);
                        await _context.SaveChangesAsync();

                        return new Response<int>(pre.DefaultValueID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}