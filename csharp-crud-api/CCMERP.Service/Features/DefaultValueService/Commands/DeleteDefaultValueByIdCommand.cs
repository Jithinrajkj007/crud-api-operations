﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.DefaultValueService.Commands
{
    public class DeleteDefaultValueByIdCommand : IRequest<Response<int>>
    {

        [Required]
        public int DefaultValueID { get; set; }

        public class DeleteDefaultValueByIdCommandHandler : IRequestHandler<DeleteDefaultValueByIdCommand, Response<int>>
        {

            private readonly IdentityContext _context;
            public DeleteDefaultValueByIdCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(DeleteDefaultValueByIdCommand request, CancellationToken cancellationToken)
            {
                try
                {
                    var defaultvalue = await _context.defaultvalues.FindAsync(request.DefaultValueID);
                    if (defaultvalue == null)
                    {
                        return new Response<int>(0, "No default value found ", false);
                    }
                    else
                    {
                        var deleteDefaultValueId = request.DefaultValueID;
                        //privilege.IsActive = 0;
                        _context.Remove(defaultvalue);
                        await _context.SaveChangesAsync();

                        return new Response<int>(deleteDefaultValueId, "default value deleted successfully", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }


            }
        }
    }
}