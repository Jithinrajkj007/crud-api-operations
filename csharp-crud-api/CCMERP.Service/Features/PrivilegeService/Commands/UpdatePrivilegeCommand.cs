﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.PrivilegeService.Commands
{
    public class UpdatePrivilegeCommand : IRequest<Response<int>>
    {
        [Required]
        public int PrivilegeID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string PrivilegeCode { get; set; }
        [Required]
        public double PrivilegeAmount { get; set; }
        [Required]
        public decimal PrivilegePrice { get; set; }
        //[Required]
        //public DateTime PrivilegeDate { get; set; }
       

        public class UpdatePrivilegeCommandHandler : IRequestHandler<UpdatePrivilegeCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdatePrivilegeCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdatePrivilegeCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var pre = _context.privilege.Where(a => a.PrivilegeID == request.PrivilegeID).FirstOrDefault();

                    if (pre == null)
                    {
                        return new Response<int>(0, "No previlege found", true);
                    }
                    else
                    {

                        pre.PrivilegeID = request.PrivilegeID;
                        pre.PrivilegeName = request.PrivilegeName;
                        pre.PrivilegeCode = request.PrivilegeCode;
                        pre.PrivilegeAmount = request.PrivilegeAmount;
                        pre.PrivilegePrice = request.PrivilegePrice;
                        //pre.PrivilegeDate = request.PrivilegeDate;
                        pre.PrivilegeDate = DateTime.Now;


                        _context.privilege.Update(pre);
                        await _context.SaveChangesAsync();

                        return new Response<int>(pre.PrivilegeID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}