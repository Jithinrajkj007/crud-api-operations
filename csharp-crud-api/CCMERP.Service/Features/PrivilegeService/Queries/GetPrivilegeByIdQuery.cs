﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Privilege.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.PrivilegeService.Queries
{
    public class GetPrivilegeByIdQuery : IRequest<Response<PrivilegeMaster>>
    {
        public int privilegeId { get; set; }

        public class GetPrivilegeByIdQueryHandler : IRequestHandler<GetPrivilegeByIdQuery, Response<PrivilegeMaster>>
        {
            private readonly IdentityContext _context;

            public GetPrivilegeByIdQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<PrivilegeMaster>> Handle(GetPrivilegeByIdQuery request, CancellationToken cancellationToken)
            {

                PrivilegeMaster privilege = new();
                try
                {

                    privilege = (from t1 in _context.privilege

                                 where t1.PrivilegeID == request.privilegeId
                                 select new PrivilegeMaster
                                 {
                                     PrivilegeID = t1.PrivilegeID,
                                     PrivilegeName = t1.PrivilegeName,
                                     PrivilegeCode = t1.PrivilegeCode,
                                     PrivilegeAmount = t1.PrivilegeAmount,
                                     PrivilegePrice = t1.PrivilegePrice,
                                     //PrivilegeDate = t1.PrivilegeDate

                                 }).FirstOrDefault();

                    if (privilege != null)
                    {
                        return await Task.FromResult(new Response<PrivilegeMaster>(privilege, "Success", true));
                    }
                    else
                    {


                        return await Task.FromResult(new Response<PrivilegeMaster>(privilege, "No record found ", false));
                    }
                }
                catch (Exception)
                {

                    return await Task.FromResult(new Response<PrivilegeMaster>(privilege, "Exception", false));
                }

            }
        }



    }
}