﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Privilege.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.PrivilegeService.Queries
{
   public class GetAllPrivilegeQuery : IRequest<Response<GetAllPrivilegeResponse>>
    {
        public GetAllPrivilegeQuery()
        {

        }
        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllPrivilegeQueryHandler : IRequestHandler<GetAllPrivilegeQuery, Response<GetAllPrivilegeResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllPrivilegeQueryHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllPrivilegeResponse>> Handle(GetAllPrivilegeQuery request, CancellationToken cancellationToken)
            {

                GetAllPrivilegeResponse getAllPrivilegeResponse = new();
                try
                {
                    var PrivilegeList = await
                        (
                        from o in _context.privilege
                        select new PrivilegeMaster()
                        {
                            PrivilegeID=o.PrivilegeID,
                            PrivilegeName=o.PrivilegeName,
                            PrivilegeCode=o.PrivilegeCode,
                            PrivilegeAmount=o.PrivilegeAmount,
                            PrivilegePrice=o.PrivilegePrice,
                            //PrivilegeDate=o.PrivilegeDate
                            PrivilegeDate = DateTime.Now,

                        }).Distinct().ToListAsync();
                    if (PrivilegeList.Count == 0)
                    {

                        return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllPrivilegeResponse.TotalItems = PrivilegeList.Count;
                        getAllPrivilegeResponse.TotalPages = (int)Math.Ceiling(getAllPrivilegeResponse.TotalItems / (double)request.PageSize);
                        getAllPrivilegeResponse.Privileges = PrivilegeList.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "Success", true);
                    }
                }
                catch (Exception)
                {

                    return new Response<GetAllPrivilegeResponse>(getAllPrivilegeResponse, "Exception", false);
                }


            }
        }



    }
}
