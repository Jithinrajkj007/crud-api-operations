﻿using CCMERP.Domain.Common;
using CCMERP.Persistence;
using MediatR;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CountryService.Commands
{
    public class UpdateCountryCommand : IRequest<Response<int>>
    {
        [Required]
        public int CountryID { get; set; }

        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string CountryName { get; set; }
        [Required]
        [MinLength(3)]
        [MaxLength(30)]
        public string ABR { get; set; }
        [Required]
        public int IsActive { get; set; }
        [Required]
        public int CountryCode { get; set; }


        public class UpdateCountryCommandHandler : IRequestHandler<UpdateCountryCommand, Response<int>>
        {
            private readonly IdentityContext _context;
            public UpdateCountryCommandHandler(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<int>> Handle(UpdateCountryCommand request, CancellationToken cancellationToken)
            {
                try
                {


                    var cou = _context.country_master.Where(a => a.CountryID == request.CountryID).FirstOrDefault();

                    if (cou == null)
                    {
                        return new Response<int>(0, "No country found", true);
                    }
                    else
                    {

                        cou.ABR = request.ABR;
                        cou.CountryCode = request.CountryCode;
                        cou.CountryName = request.CountryName;
                        cou.IsActive = request.IsActive;


                        _context.country_master.Update(cou);
                        await _context.SaveChangesAsync();

                        return new Response<int>(cou.CountryID, "Success", true);
                    }
                }
                catch (Exception)
                {
                    return new Response<int>(0, "Exception", false);
                }
            }
        }


    }
}
