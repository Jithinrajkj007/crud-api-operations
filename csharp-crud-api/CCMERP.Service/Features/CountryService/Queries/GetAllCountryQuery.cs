﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Country.Response;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.CountryService.Queries
{
    public class GetAllCountryQuery : IRequest<Response<GetAllCountryResponse>>
    {

        public GetAllCountryQuery()
        {

        }

        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }



        public class GetAllCountryOneQuery : IRequestHandler<GetAllCountryQuery, Response<GetAllCountryResponse>>
        {
            private readonly IdentityContext _context;
            public GetAllCountryOneQuery(IdentityContext context)
            {
                _context = context;
            }
            public async Task<Response<GetAllCountryResponse>> Handle(GetAllCountryQuery request, CancellationToken cancellationToken)
            {

                GetAllCountryResponse getAllCountryResponse = new GetAllCountryResponse();
                try
                {
                    var CountryList = await
                        (
                        from o in _context.country_master
                        orderby o.CountryName ascending

                        select new CountryMaster()
                        {
                            ABR=o.ABR,
                            CountryCode=o.CountryCode,
                            CountryID=o.CountryID,
                            CountryName=o.CountryName,
                            IsActive=o.IsActive

                           
                        }).Distinct().ToListAsync();
                    if (CountryList.Count == 0)
                    {

                        return new Response<GetAllCountryResponse>(getAllCountryResponse, "No record found ", false);
                    }
                    else
                    {
                        getAllCountryResponse.TotalItems = CountryList.Count;
                        getAllCountryResponse.TotalPages = (int)Math.Ceiling(getAllCountryResponse.TotalItems / (double)request.PageSize);
                        getAllCountryResponse.Countries = CountryList. Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllCountryResponse>(getAllCountryResponse, "Success", true);
                    }
                }
                catch (Exception)
                {

                    return new Response<GetAllCountryResponse>(getAllCountryResponse, "Exception", false);
                }


            }
        }


    }
}
