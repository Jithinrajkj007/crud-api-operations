﻿using CCMERP.Domain.Common;
using CCMERP.Domain.Customers.Response;
using CCMERP.Domain.Entities;
using CCMERP.Persistence;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using System.Linq.Expressions;

namespace CCMERP.Service.Features.CustomerFeatures.Queries
{
    public class GetAllCustomerQuery : IRequest<Response<GetAllCustomersResponse>>
    {
        public GetAllCustomerQuery()
        {
            salesRepId = 0;
        }
        public int orgId { get; set; }
        public int salesRepId { get; set; }

        const int maxPageSize = 50;
        public int PageNumber { get; set; }
        public int _pageSize { get; set; }
        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }

        public class GetAllCustomerQueryHandler : IRequestHandler<GetAllCustomerQuery, Response<GetAllCustomersResponse>>
        {
            private readonly ITransactionDbContext _context;
            private readonly IdentityContext _icontext;
            public GetAllCustomerQueryHandler(ITransactionDbContext context, IdentityContext icontext)
            {
                _context = context;
                _icontext = icontext;
            }
            public async Task<Response<GetAllCustomersResponse>> Handle(GetAllCustomerQuery request, CancellationToken cancellationToken)
            {

                GetAllCustomersResponse customersResponse = new GetAllCustomersResponse();
                List<Customer> customers = new List<Customer>();
                try
                {
                    if (request.salesRepId == 0)
                    {
                        customers = await (from c in _context.Customers
                                           join co in _context.organizationCustomerMappings on c.CustomerID equals co.CustomerID
                                           where co.Org_ID == request.orgId
                                           //orderby c.Name ascending
                                           select new Customer()
                                           {
                                               CustomerID = c.CustomerID,
                                               Name = c.Name,
                                               VatID = c.VatID,
                                               ShippingAddress1 = c.ShippingAddress1,
                                               ShippingAddress2 = c.ShippingAddress2,
                                               ShippingCity = c.ShippingCity,
                                               ShippingState = c.ShippingState,
                                               ShippingCountry = c.ShippingCountry,
                                               ShippingZipCode = c.ShippingZipCode,
                                               BillingAddress1 = c.BillingAddress1,
                                               BillingAddress2 = c.BillingAddress2,
                                               BillingCity = c.BillingCity,
                                               BillingState = c.BillingState,
                                               BillingCountry = c.BillingCountry,
                                               BillingZipCode = c.BillingZipCode,
                                               CurrencyId = c.CurrencyId,
                                               ContactPerson = c.ContactPerson,
                                               ContactPosition = c.ContactPosition,
                                               ContactNumber = c.ContactNumber,
                                               ContactEmail = c.ContactEmail,
                                               orgId = co.Org_ID,
                                               userId = co.User_ID,
                                               IsActive = co.IsActive
                                           }).ToListAsync();
                    }
                    else
                    {
                        customers = await (from c in _context.Customers
                                           join co in _context.organizationCustomerMappings on c.CustomerID equals co.CustomerID
                                           where co.Org_ID == request.orgId && co.SalesRepId==request.salesRepId
                                           select new Customer()
                                           {
                                               CustomerID = c.CustomerID,
                                               Name = c.Name,
                                               VatID = c.VatID,
                                               ShippingAddress1 = c.ShippingAddress1,
                                               ShippingAddress2 = c.ShippingAddress2,
                                               ShippingCity = c.ShippingCity,
                                               ShippingState = c.ShippingState,
                                               ShippingCountry = c.ShippingCountry,
                                               ShippingZipCode = c.ShippingZipCode,
                                               BillingAddress1 = c.BillingAddress1,
                                               BillingAddress2 = c.BillingAddress2,
                                               BillingCity = c.BillingCity,
                                               BillingState = c.BillingState,
                                               BillingCountry = c.BillingCountry,
                                               BillingZipCode = c.BillingZipCode,
                                               CurrencyId = c.CurrencyId,
                                               ContactPerson = c.ContactPerson,
                                               ContactPosition = c.ContactPosition,
                                               ContactNumber = c.ContactNumber,
                                               ContactEmail = c.ContactEmail,
                                               orgId = co.Org_ID,
                                               userId = co.User_ID,
                                               IsActive = co.IsActive
                                           }).ToListAsync();
                    }
                    if (customers.Count == 0)
                    {
                        
                        return new Response<GetAllCustomersResponse>(customersResponse, "No record found ", false);
                    }
                    else
                    {
                        customersResponse.TotalItems = customers.Count;
                        customersResponse.TotalPages = (int)Math.Ceiling(customersResponse.TotalItems / (double)request.PageSize);
                        customersResponse.customers = customers.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();
                        return new Response<GetAllCustomersResponse>(customersResponse, "Success", true);
                    }
                }
                catch (Exception ex)
                {

                    return new Response<GetAllCustomersResponse>(customersResponse, "Exception", false);
                }


            }
        }
    }
}
