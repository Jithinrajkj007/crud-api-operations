﻿using CCMERP.Domain.AddItem.Request;
using CCMERP.Domain.Common;
using CCMERP.Domain.Entities;
using CCMERP.Domain.Order.Data;
using CCMERP.Domain.Order.Response;
using CCMERP.Persistence;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CCMERP.Service.Features.Order.Queries
{
    public class GetallOrderItemByHeaderIdQuery : IRequest<Response<GetallOrderItemByHeaderIdResponse>>
    {
        public int orgId { get; set; }
        public int headerId { get; set; }
        //const int maxPageSize = 50;
        //public int PageNumber { get; set; }
        //public int _pageSize { get; set; }
        //public int PageSize
        //{
        //    get
        //    {
        //        return _pageSize;
        //    }
        //    set
        //    {
        //        _pageSize = (value > maxPageSize) ? maxPageSize : value;
        //    }
        //}
        public class GetallOrderItemByHeaderIdQueryHandler : IRequestHandler<GetallOrderItemByHeaderIdQuery, Response<GetallOrderItemByHeaderIdResponse>>
        {
            private readonly TransactionDbContext _context;
            private readonly IdentityContext _icontext;
            public GetallOrderItemByHeaderIdQueryHandler(TransactionDbContext context, IdentityContext icontext)
            {
                _context = context;
                _icontext = icontext;
            }
            public async Task<Response<GetallOrderItemByHeaderIdResponse>> Handle(GetallOrderItemByHeaderIdQuery request, CancellationToken cancellationToken)
            {

                GetallOrderItemByHeaderIdResponse GetallItemByIdResponse = new GetallOrderItemByHeaderIdResponse();
                SalesOrderHeaderModel salesOrderHeader = new SalesOrderHeaderModel();
                try
                {
                    var country = _icontext.country_master.ToList();
                    var curency = _icontext.currency_master.ToList();
                    var cust = _context.salesorderheader.ToList();
                    var org = _context.organizationCustomerMappings.ToList();

                    GetallItemByIdResponse.salesOrderHeaderModel = (from t1 in cust
                                        join t2 in country on t1.BillingCountry equals t2.CountryID
                                        join t3 in country on t1.ShippingCountry equals t3.CountryID
                                        join t5 in org on t1.OrgId equals t5.Org_ID
                                        join t4 in curency on t1.CurrencyId equals t4.CurrencyID
                                        where t1.OrgId== request.orgId && t1.SOHdrId == request.headerId
                                        select new SalesOrderHeaderModel
                                        {
                                            shippingAddress1 = t1.ShippingAddress1,
                                            shippingAddress2 = t1.ShippingAddress2,
                                            shippingCity = t1.ShippingCity,
                                            shippingState = t1.ShippingState,
                                            shippingCountry = t1.ShippingCountry,
                                            shippingCountryName = t3.CountryName,
                                            shippingZipCode = t1.ShippingZipCode,
                                            billingAddress1 = t1.BillingAddress1,
                                            billingAddress2 = t1.BillingAddress2,
                                            billingCity = t1.BillingCity,
                                            billingState = t1.BillingState,
                                            billingCountry = t1.BillingCountry,
                                            billingCountryName = t2.CountryName,
                                            billingZipCode = t1.BillingZipCode,
                                            currencyId = t1.CurrencyId,
                                            currency = t4.CurrencyName,
                                            expectedDate = t1.ExpectedDate.ToString("dd-MM-yyyy"),
                                            orgId = request.orgId,
                                            soDate = t1.SODate.ToString("dd-MM-yyyy"),
                                            soNo = t1.SONo,
                                            status = getSalesorderSataus(t1.StatusId),
                                            statusId = t1.StatusId,
                                            sohdrId=t1.SOHdrId,
                                            customerId=t1.CustomerId
                                        }).FirstOrDefault();

                    if (GetallItemByIdResponse.salesOrderHeaderModel==null)
                    {
                       
                        return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "No record found ", false));
                    }
                    else
                    {
                        List<SalesOrderDeatils> salesOrderitems =(from a in _context.salesorderdtl join b in _context.itemmaster on a.ItemId equals(b.ItemId) where a.SOHdrId == request.headerId && a.OrgId ==request.orgId select new SalesOrderDeatils()
                        {
                            itemid = a.ItemId,
                            itemCode = b.ItemCode,
                            itemName = b.ItemName,
                            uomid = b.UOMID,
                            uomName = (_context.uOMMaster.Where(b => b.UOMId == b.UOMId).FirstOrDefault().Name),
                            basePrice = b.BasePrice,
                            minOrderQty = b.MinOrderQty,
                            orgid = a.OrgId,
                            expectedDate=a.ExpectedDate.ToString("dd-MM-yyyy"),
                            qtyRequired=a.Quantity

                        }).ToList();

                        if (salesOrderitems.Count > 0)
                        {

                            GetallItemByIdResponse.salesOrderitems = salesOrderitems;

                        }
                   

                        //GetallItemByIdResponse.TotalItems = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Count;
                        //GetallItemByIdResponse.TotalPages = (int)Math.Ceiling(GetallItemByIdResponse.TotalItems / (double)request.PageSize);
                        //GetallItemByIdResponse.getallOrderByCustomerIdDatas = GetallItemByIdResponse.getallOrderByCustomerIdDatas.Skip((request.PageNumber - 1) * request.PageSize).Take(request.PageSize).ToList();

                        return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "Success", true));
                    }
                }
                catch (Exception ex)
                {
                    return await Task.FromResult(new Response<GetallOrderItemByHeaderIdResponse>(GetallItemByIdResponse, "Exception", false));
                }


            }
        }
        public static string getSalesorderSataus(int statusId)
        {
            string status = string.Empty;

            try
            {
                switch (statusId)
                {
                    case 1:
                        status = "Draft Mode";
                        break;
                    case 2:
                        status = "Order Confirm";
                        break;
                        //case 3:
                        //    Console.WriteLine("Wednesday");
                        //    break;
                        //case 4:
                        //    Console.WriteLine("Thursday");
                        //    break;
                        //case 5:
                        //    Console.WriteLine("Friday");
                        //    break;
                        //case 6:
                        //    Console.WriteLine("Saturday");
                        //    break;
                        //case 7:
                        //    Console.WriteLine("Sunday");
                        //    break;
                }

            }
            catch (Exception)
            {


            }
            return status;
        }
    }
}