﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using MySql.EntityFrameworkCore.Metadata;

namespace CCMERP.Persistence.Migrations.Application
{
    public partial class InitialcommitApplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "ccmtransdb");

            migrationBuilder.CreateTable(
                name: "Customers",
                schema: "ccmtransdb",
                columns: table => new
                {
                    CustomerID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "varchar(250)", nullable: true),
                    VatID = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingAddress1 = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingAddress2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingCity = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingState = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingCountry = table.Column<int>(type: "int", nullable: false),
                    ShippingZipCode = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingAddress1 = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingAddress2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingCity = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingState = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingCountry = table.Column<int>(type: "int", nullable: false),
                    BillingZipCode = table.Column<string>(type: "varchar(250)", nullable: true),
                    CurrencyId = table.Column<int>(type: "int", nullable: false),
                    ContactPerson = table.Column<string>(type: "varchar(250)", nullable: true),
                    ContactPosition = table.Column<string>(type: "varchar(250)", nullable: true),
                    ContactNumber = table.Column<string>(type: "varchar(250)", nullable: true),
                    ContactEmail = table.Column<string>(type: "varchar(250)", nullable: true),
                    IsActive = table.Column<short>(type: "smallint", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedByUser = table.Column<int>(type: "int", nullable: false),
                    CreatedByProgram = table.Column<string>(type: "varchar(250)", nullable: true),
                    LastModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    LastModifiedBy = table.Column<int>(type: "int", nullable: false),
                    LastModifiedByProgram = table.Column<string>(type: "varchar(250)", nullable: true),
                    ExternalReference = table.Column<string>(type: "varchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustomerID);
                });

            migrationBuilder.CreateTable(
                name: "itemmaster",
                schema: "ccmtransdb",
                columns: table => new
                {
                    ItemId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    OrgId = table.Column<int>(type: "int", nullable: false),
                    ItemCode = table.Column<string>(type: "varchar(250)", nullable: true),
                    ItemName = table.Column<string>(type: "varchar(250)", nullable: true),
                    BasePrice = table.Column<double>(type: "double", nullable: false),
                    CatgeoryId = table.Column<int>(type: "int", nullable: false),
                    UOMID = table.Column<int>(type: "int", nullable: false),
                    MinOrderQty = table.Column<int>(type: "int", nullable: false),
                    CreatedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    CreatedBy = table.Column<int>(type: "int", nullable: false),
                    ModifiedBy = table.Column<int>(type: "int", nullable: false),
                    ModifiedDate = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_itemmaster", x => x.ItemId);
                });

            migrationBuilder.CreateTable(
                name: "keymaster",
                schema: "ccmtransdb",
                columns: table => new
                {
                    KeyId = table.Column<int>(type: "int", nullable: false),
                    OrgId = table.Column<int>(type: "int", nullable: false),
                    Value = table.Column<long>(type: "bigint", nullable: false),
                    Description = table.Column<string>(type: "varchar(250)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_keymaster", x => new { x.KeyId, x.OrgId });
                });

            migrationBuilder.CreateTable(
                name: "organizationCustomerMappings",
                schema: "ccmtransdb",
                columns: table => new
                {
                    Org_ID = table.Column<int>(type: "int", nullable: false),
                    CustomerID = table.Column<int>(type: "int", nullable: false),
                    User_ID = table.Column<int>(type: "int", nullable: false),
                    SalesRepId = table.Column<int>(type: "int", nullable: false),
                    IsActive = table.Column<short>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_organizationCustomerMappings", x => new { x.Org_ID, x.CustomerID });
                });

            migrationBuilder.CreateTable(
                name: "salesorderdtl",
                schema: "ccmtransdb",
                columns: table => new
                {
                    SODtlId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    OrgId = table.Column<int>(type: "int", nullable: false),
                    SOHdrId = table.Column<int>(type: "int", nullable: false),
                    ItemId = table.Column<int>(type: "int", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    ExpectedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Remarks = table.Column<string>(type: "varchar(250)", nullable: true),
                    StatusId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_salesorderdtl", x => x.SODtlId);
                });

            migrationBuilder.CreateTable(
                name: "salesorderheader",
                schema: "ccmtransdb",
                columns: table => new
                {
                    SOHdrId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    OrgId = table.Column<int>(type: "int", nullable: false),
                    CustomerId = table.Column<int>(type: "int", nullable: false),
                    SONo = table.Column<string>(type: "varchar(250)", nullable: true),
                    SODate = table.Column<DateTime>(type: "datetime", nullable: false),
                    ExpectedDate = table.Column<DateTime>(type: "datetime", nullable: false),
                    Remarks = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingAddress1 = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingAddress2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingCity = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingState = table.Column<string>(type: "varchar(250)", nullable: true),
                    ShippingCountry = table.Column<int>(type: "int", nullable: false),
                    ShippingZipCode = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingAddress1 = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingAddress2 = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingCity = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingState = table.Column<string>(type: "varchar(250)", nullable: true),
                    BillingCountry = table.Column<int>(type: "int", nullable: false),
                    BillingZipCode = table.Column<string>(type: "varchar(250)", nullable: true),
                    CurrencyId = table.Column<int>(type: "int", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_salesorderheader", x => x.SOHdrId);
                });

            migrationBuilder.CreateTable(
                name: "uOMMaster",
                schema: "ccmtransdb",
                columns: table => new
                {
                    UOMId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    OrgId = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "varchar(250)", nullable: true),
                    IsActive = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_uOMMaster", x => x.UOMId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Customers",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "itemmaster",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "keymaster",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "organizationCustomerMappings",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "salesorderdtl",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "salesorderheader",
                schema: "ccmtransdb");

            migrationBuilder.DropTable(
                name: "uOMMaster",
                schema: "ccmtransdb");
        }
    }
}
