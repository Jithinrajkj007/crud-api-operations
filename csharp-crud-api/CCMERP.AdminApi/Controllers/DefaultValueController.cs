﻿using CCMERP.Service.Features.DefaultValueService.Commands;
using CCMERP.Service.Features.DefaultValueService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;


namespace CCMERP.AdminApi.Controllers
{
    [Route("api/V2/DefaultValues")]
    [ApiController]
    public class DefaultValueController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();


        [HttpPost]
        public async Task<IActionResult> Create(CreateDefaultValuesCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpGet]
        //[Authorize]
        public async Task<IActionResult> GetAll(int pageNumber = 1, int pageSize = 25)
        {
            return Ok(await Mediator.Send(new GetAllDefaultValuesQuery { PageNumber = pageNumber, _pageSize = pageSize }));
        }

        [HttpGet("{DefaultValueID}")]
        public async Task<IActionResult> GetById(int DefaultValueID)
        {
            return Ok(await Mediator.Send(new GetDefaultValuesByIdQuery { DefaultValueID = DefaultValueID }));
        }
      
       [HttpDelete("{DefaultValueID}")]
       public async Task<IActionResult> DeleteDefaultValue(int DefaultValueID)
       {
           return Ok(await Mediator.Send(new DeleteDefaultValueByIdCommand { DefaultValueID = DefaultValueID }));
       }

        
             [HttpPut]
             public async Task<IActionResult> Update(UpdateDefaultValueCommand command)
             {
                 return Ok(await Mediator.Send(command));
             }
    }
}

