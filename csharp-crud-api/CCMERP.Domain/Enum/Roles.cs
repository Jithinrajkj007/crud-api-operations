﻿using System;

namespace CCMERP.Domain.Enum
{
    public enum Roles
    {
        SuperAdmin,
        ClientAdmin,
        SalesRep,
        Customer
    }

    public static class Constants
    {
        public static readonly int SuperAdmin = 1;
        public static readonly int ClientAdmin = 2;
        public static readonly int SalesRep = 3;
        public static readonly int Customer = 4;
        public static readonly int SuperAdminUser = 1;
        public static readonly int CustomerUser = 1;
    }


}
