﻿using System.Collections.Generic;

namespace CCMERP.Domain.Common
{
    public class Response<T>
    {
        private string v1;
        private bool v2;

        public Response()
        {
        }

        public Response(string v1, bool v2)
        {
            this.v1 = v1;
            this.v2 = v2;
        }

        public Response(T data, string message = null,bool _succeeded=false)
        {
            Status = _succeeded;
            Message = message;
            Data = data;
        }
        public Response(object privilegeID, string message, bool _succeeded = false)
        {
            Status = _succeeded;
            Message = message;
        }
        public bool Status { get; set; }
        public string Message { get; set; }
        public List<string> Errors { get; set; }
        public T Data { get; set; }
    }
}
