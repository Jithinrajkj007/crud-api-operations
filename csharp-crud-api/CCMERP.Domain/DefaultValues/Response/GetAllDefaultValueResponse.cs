﻿using System;
using System.Collections.Generic;
using CCMERP.Domain.Pagination.Models;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CCMERP.Domain.Entities;

namespace CCMERP.Domain.DefaultValues
{
    public class GetAllDefaultValueResponse : PaginationResponse
    {
        public List<DefaultValue> DefaultValues { get; set; }
    }
}
