﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Entities
{
    public class DefaultValue
    {
        [Key]
        public int DefaultValueID { get; set; }
        public string DefaultName { get; set; }
        public decimal DefaultValue1 { get; set; }
        public float DefaultValue2 { get; set; }
        public double DefaultValue3 { get; set; }
        public DateTime DefaultDate { get; set; }
        public string DefaultValue4 { get; set; }
        public string DefaultTime { get; set; }
        public string DefaultCity { get; set; }
        public long DefaultPhone { get; set; }
        public byte DefaultID { get; set; }

        public int DefaultValue5 { get; set; }
        public int DefaultValue6 { get; set; }
        public string DefaultYear { get; set; }

        public string DefaultText { get; set; }
        public string DefaultText2 { get; set; }
        public string DefaultText3 { get; set; }

    }
}
