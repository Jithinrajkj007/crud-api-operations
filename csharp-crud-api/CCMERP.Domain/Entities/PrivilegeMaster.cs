﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCMERP.Domain.Entities
{
     public class PrivilegeMaster
    {
        [Key]
        public int PrivilegeID { get; set; }
        public string PrivilegeName { get; set; }
        public string PrivilegeCode { get; set; }
        public double PrivilegeAmount { get; set; }
        public decimal PrivilegePrice { get; set; }
        public DateTime PrivilegeDate { get; set; }

    }
}
