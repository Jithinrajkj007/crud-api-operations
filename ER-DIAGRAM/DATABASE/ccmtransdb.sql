CREATE DATABASE  IF NOT EXISTS `ccmtransdb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ccmtransdb`;
-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ccmtransdb
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `CustomerID` int NOT NULL AUTO_INCREMENT,
  `Name` varchar(250) DEFAULT NULL,
  `VatID` varchar(250) DEFAULT NULL,
  `ShippingAddress1` varchar(250) DEFAULT NULL,
  `ShippingAddress2` varchar(250) DEFAULT NULL,
  `ShippingCity` varchar(250) DEFAULT NULL,
  `ShippingState` varchar(250) DEFAULT NULL,
  `ShippingCountry` int NOT NULL,
  `ShippingZipCode` varchar(250) DEFAULT NULL,
  `BillingAddress1` varchar(250) DEFAULT NULL,
  `BillingAddress2` varchar(250) DEFAULT NULL,
  `BillingCity` varchar(250) DEFAULT NULL,
  `BillingState` varchar(250) DEFAULT NULL,
  `BillingCountry` int NOT NULL,
  `BillingZipCode` varchar(250) DEFAULT NULL,
  `CurrencyId` int NOT NULL,
  `ContactPerson` varchar(250) DEFAULT NULL,
  `ContactPosition` varchar(250) DEFAULT NULL,
  `ContactNumber` varchar(250) DEFAULT NULL,
  `ContactEmail` varchar(250) DEFAULT NULL,
  `IsActive` smallint NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedByUser` int NOT NULL,
  `CreatedByProgram` varchar(250) DEFAULT NULL,
  `LastModifiedDate` datetime NOT NULL,
  `LastModifiedBy` int NOT NULL,
  `LastModifiedByProgram` varchar(250) DEFAULT NULL,
  `ExternalReference` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`CustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `itemmaster`
--

DROP TABLE IF EXISTS `itemmaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `itemmaster` (
  `ItemId` int NOT NULL AUTO_INCREMENT,
  `OrgId` int NOT NULL,
  `ItemCode` varchar(250) DEFAULT NULL,
  `ItemName` varchar(250) DEFAULT NULL,
  `BasePrice` double NOT NULL,
  `CatgeoryId` int NOT NULL,
  `UOMID` int NOT NULL,
  `MinOrderQty` int NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `CreatedBy` int NOT NULL,
  `ModifiedBy` int NOT NULL,
  `ModifiedDate` datetime NOT NULL,
  PRIMARY KEY (`ItemId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `itemmaster`
--

LOCK TABLES `itemmaster` WRITE;
/*!40000 ALTER TABLE `itemmaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemmaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `keymaster`
--

DROP TABLE IF EXISTS `keymaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `keymaster` (
  `KeyId` int NOT NULL,
  `OrgId` int NOT NULL,
  `Value` bigint NOT NULL,
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`KeyId`,`OrgId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keymaster`
--

LOCK TABLES `keymaster` WRITE;
/*!40000 ALTER TABLE `keymaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `keymaster` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizationcustomermappings`
--

DROP TABLE IF EXISTS `organizationcustomermappings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizationcustomermappings` (
  `Org_ID` int NOT NULL,
  `CustomerID` int NOT NULL,
  `User_ID` int NOT NULL,
  `SalesRepId` int NOT NULL,
  `IsActive` smallint NOT NULL,
  PRIMARY KEY (`Org_ID`,`CustomerID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizationcustomermappings`
--

LOCK TABLES `organizationcustomermappings` WRITE;
/*!40000 ALTER TABLE `organizationcustomermappings` DISABLE KEYS */;
/*!40000 ALTER TABLE `organizationcustomermappings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesorderdtl`
--

DROP TABLE IF EXISTS `salesorderdtl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salesorderdtl` (
  `SODtlId` int NOT NULL AUTO_INCREMENT,
  `OrgId` int NOT NULL,
  `SOHdrId` int NOT NULL,
  `ItemId` int NOT NULL,
  `Quantity` int NOT NULL,
  `ExpectedDate` datetime NOT NULL,
  `Remarks` varchar(250) DEFAULT NULL,
  `StatusId` int NOT NULL,
  PRIMARY KEY (`SODtlId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesorderdtl`
--

LOCK TABLES `salesorderdtl` WRITE;
/*!40000 ALTER TABLE `salesorderdtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesorderdtl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salesorderheader`
--

DROP TABLE IF EXISTS `salesorderheader`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salesorderheader` (
  `SOHdrId` int NOT NULL AUTO_INCREMENT,
  `OrgId` int NOT NULL,
  `CustomerId` int NOT NULL,
  `SONo` varchar(250) DEFAULT NULL,
  `SODate` datetime NOT NULL,
  `ExpectedDate` datetime NOT NULL,
  `Remarks` varchar(250) DEFAULT NULL,
  `ShippingAddress1` varchar(250) DEFAULT NULL,
  `ShippingAddress2` varchar(250) DEFAULT NULL,
  `ShippingCity` varchar(250) DEFAULT NULL,
  `ShippingState` varchar(250) DEFAULT NULL,
  `ShippingCountry` int NOT NULL,
  `ShippingZipCode` varchar(250) DEFAULT NULL,
  `BillingAddress1` varchar(250) DEFAULT NULL,
  `BillingAddress2` varchar(250) DEFAULT NULL,
  `BillingCity` varchar(250) DEFAULT NULL,
  `BillingState` varchar(250) DEFAULT NULL,
  `BillingCountry` int NOT NULL,
  `BillingZipCode` varchar(250) DEFAULT NULL,
  `CurrencyId` int NOT NULL,
  `StatusId` int NOT NULL,
  PRIMARY KEY (`SOHdrId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salesorderheader`
--

LOCK TABLES `salesorderheader` WRITE;
/*!40000 ALTER TABLE `salesorderheader` DISABLE KEYS */;
/*!40000 ALTER TABLE `salesorderheader` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `uommaster`
--

DROP TABLE IF EXISTS `uommaster`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `uommaster` (
  `UOMId` int NOT NULL AUTO_INCREMENT,
  `OrgId` int NOT NULL,
  `Name` varchar(250) DEFAULT NULL,
  `IsActive` int NOT NULL,
  PRIMARY KEY (`UOMId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `uommaster`
--

LOCK TABLES `uommaster` WRITE;
/*!40000 ALTER TABLE `uommaster` DISABLE KEYS */;
/*!40000 ALTER TABLE `uommaster` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-08-06 12:36:04
